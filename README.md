# JSON Messaging პროტოკოლის აღწერა #

- [ძირი პრინციპები](basePrincipals.txt)
- [ვერსიების ისტორია](versions.txt)
- **[JSONMEssaging.ts სტანდარტის აღწერა, სახელმძღვანელო კოდი](JSONMessaging.ts)**
- [JSONMessaging კლიენტური ბიბლიოთეკის სახელმძღვანელო კოდი](JSONMessagingClientReferenceCode.ts)