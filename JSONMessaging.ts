/// <reference path="RequestReponseIDLDataTypes.ts" />

//
// JSON მესიჯის ობიექტის საბაზო ტიპი
//
// JSONMessage.messageId: 
// გამოიყენება
// - ასინქრონული 1-ზე მეტი ერთი და იგივე მაგრამ სხვა და სხვა პარამეტრების 
//   ქმონე request-ებზე response-ების ფილტრაციისათვის
// - გამოიყენება ტრანზქციული მესიჯინგის რეალიზებისთვის, კერძოდ: უწინ გაგზავნილი 
//   messageId-ის მგონი მესიჯის დამუშავების სისრულის გადამოწმება
// - ასევე ერთი გაგზავნილ request-ზე შეიძლება რამდენიმე სხვადასხვა სახელის მქონე 
//   response მესიჯი მოდიოდეს, აქ კვლავ request.messageId-ი არის ის რის საფუძველზეც შეიძლება request-response-ს შესაბამისობების დადგენა
//
// JSONMessage.type: 
// - ასევე request response-ებს შორის კავშირის დამყარების საშუალებაა, ასინქრონულ რეჟიმში მრავალი request-ის გზავნისას.
// - ერთი და იგივე JSONMessage.messageId მნიშვნელობისათვის, JSONMessage.type-ით ხდება დადგენა, მესიჯი პირველადია თუ პასუხია რაიმე წინ მსწრებ მესიჯზე.
//

interface JSONMessage {
    method: JSONMethod | number;
    params: JSONMessageParams;
    messageId?: number | string;
    type?: JSONMessageType;
    errorCode?: ErrorCode | Integer;

    // ეს ველი ისაზღვრება იმ შემთხვევაში თუ მესიჯი გვინდა რომ ტრანზქციულად
    // დამუშავდეს სერვერზე. თუ null-ია მაშინ მესიჯი ტრანზაქციული არაა
    transaction?: JSONMessageTransaction
}

interface JSONMessageExt1<T extends JSONMessageParams> extends JSONMessage {
    params: T
}

type SessionId = string
type Base64 = string

type Decimal = number
type Integer = number

namespace DocHelpers {

    export interface RequestResponseCouple {
        requestMessage: JSONMessage
        responseMessage: JSONMessage
    }

}


//
// მეთოდების ჩამონათვალი
//
enum JSONMethod {
    Reserved0 = 0, // დარეზერვებული
    Reserved1 = 1, // დარეზერვებული
    Reserved2 = 2, // დარეზერვებული
    Reserved3 = 3, // დარეზერვებული
    Reserved4 = 4, // დარეზერვებული
    Reserved5 = 5, // დარეზერვებული
    Reserved6 = 6, // დარეზერვებული
    Reserved7 = 7, // დარეზერვებული
    Reserved8 = 8, // დარეზერვებული
    Reserved9 = 9, // დარეზერვებული 9-მდე, ჩათვლით

    // კერძო მეთოდების მაგალითები
    // Login = 10,
    // LoginResult = 11 

    RegisterUser = 10,
    RegisterUserResult = 11
}

//
// მეთოდის ტიპების ჩამონათვალი:
//
enum JSONMessageType {
    Request = 0,
    Response = 1,
    Notification = 2, // მაგალითად ჩეთის მესიჯი. სერვერი აგზავნის ChatMessage მესიჯს, რომელზეც response-ს არ ელის.
}

//
// საბაზო, პარამეტრების ობიექტი:
//
interface JSONMessageParams extends Object {
    sessionId?: string
}

interface JSONMessageTransaction {
    // თუ ხდება ისე რომ უწინ გაგზავნილ მესიჯზე, რომელზეც რაიმე მიზეზის გამო დროულად ვერ
    // მივიღეთ შედეგი და მისი ტრანზქციის მდგომარეობა, გვინდა ტრანზაციის სტატუს გადამოწმება
    // მაგრამ თუ სერვერამდე არ მიუღწევია ასეთი მესიჯს არ გვინდა რომ სერვერმა დასამუშავებელი 
    // მესიჯების რიგში ჩააგდოს მოცემული მესიჯი, მაშინ ამ ველს ფუტოლებთ true-ს
    dontAddToQueueIfNonExisted: Boolean

    transactionState: JSONMessageTransactionState
}

//
// ტრანზაქციების უზრუნველყოფა
// 
enum JSONMessageTransactionState {
    Pending = 2,
    Failed = 3,
    Finished = 4
}

//
// სისტემის რანგის შეცდომების, განვრცობადი ჩამონათვალი
// 
enum ErrorCode {
    None = 0,
    UnknowError = 1,
    UnhandledException = 2,

    Reserved0 = 3,
    Reserved1 = 4,
    Reserved2 = 5,
    Reserved3 = 6,
    Reserved4 = 7,
    Reserved5 = 8,
    Reserved6 = 9,



}

declare function GenerateJSONMessageID(): number;
declare function CopyJSONMessageIDFromRequestMessage(): number;
declare function GenerateNewSessionId(): number

//
// მაგალითები
//
namespace Examples {

    //
    // JSON მესიჯის ობიექტის კონკრეტული მაგალითი:
    //
    var loginRequest = {
        method: 10, //JSONMethod.Login = 10
        params: {
            userName: "abcd",
            password: "!23ca"
        },
        messageId: 14,
        type: 0, //JSONMessageType.Request
        errorCode: ErrorCode.None
    }


    var loginResponse = {
        method: 11, //JSONMethod.LoginResult = 11
        params: {
            success: true,
            sessionId: "131232kfld",
        },
        messageId: 14,
        type: 1, //JSONMessageType.Request
        errorCode: ErrorCode.None
    }

    namespace DocumentationExample {
        var _: DocHelpers.RequestResponseCouple

        _ = {
            requestMessage: <JSONMessageExt1<{
                userName:string,
                password:string
            }>>{
                method: JSONMethod.RegisterUser
            },

            responseMessage: <JSONMessageExt1<{
                sessionId:string
            }>>{
                method: JSONMethod.RegisterUserResult
            }
        }
    }


}