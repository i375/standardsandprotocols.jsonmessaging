/// <reference path="JSONMessaging.ts" />

enum JSONAPIImplementationStatus
{
    Finished,
    Queued,
    InProgress,
    FinishedNeedsRestructuring,
    FinishedNeedUpdatesToComplyWithChanges
}

interface JSONMessageAPIDocumentation
{
    implementationStatus:JSONAPIImplementationStatus
    requestMessage:JSONMessage
    responseMessage?:JSONMessage
    possibleResponseMessages?:Array<JSONMessage>
}